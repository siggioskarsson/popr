import { Template } from "meteor/templating";
import accounting from 'accounting';
import moment from 'moment/moment';

const currencySymbols = {
    'SEK': 'kr',
    'EUR': '€',
    'CHF': 'Fr.',
    'PLN': 'zł',
    'RON': 'Leu',
    'DKK': 'kr',
    'NOK': 'kr',
    'GBP': '£',
    'CZK': 'Kč',
    'BGN': 'лв',
    'HRK': 'kn',
    'ISK': 'kr.',
    'HUF': 'Ft'
};

const calculateFee = function(vatAmount) {
    const serviceFee = Meteor.settings.public.standardServiceFee || 30;
    const minFee = Meteor.settings.public.standardMinFee || 250;
    const maxFee = Meteor.settings.public.standardMaxFee || 8000;
    let fee = Math.round(vatAmount * (serviceFee / 100));
    if (fee < minFee) {
        fee = minFee;
    } else if (fee > maxFee) {
        fee = maxFee;
    }

    return fee;
};

export const templateHelpers = {
    getLoading () {
        return Template.instance().loading.get();
    },
    getReceipt () {
        return Template.instance().receipt.get();
    },
    getDate() {
        let mDate = moment(new Date(this.time*1000));
        return mDate.isValid() ? mDate.format("DD-MM-YY HH:mm") : '';
    },
    formatDate(date) {
        let mDate = moment(new Date(date));
        return mDate.isValid() ? mDate.format("DD-MM-YY HH:mm") : '';
    },
    getCurrencySymbol(currency) {
        return currencySymbols[currency] || "€";
    },
    formatCurrency(amount, currency) {
        let currencySymbol = currencySymbols[currency] || "€";
        let decimals = 2;
        // all currency values are *100 to avoid decimals in database
        let value = Number(amount) / 100;
        if (value < 0.01 && value > 0) {
            // http://stackoverflow.com/a/31002148
            let numberOfZerosAfterDecimal = (-Math.floor( Math.log(value) / Math.log(10) + 1));
            return accounting.formatMoney(value, currencySymbol + " ", numberOfZerosAfterDecimal + 1, ",", ".");
        }
        return accounting.formatMoney(value, currencySymbol + " ", decimals, ",", ".");
    },
    formatAmount(amount) {
        let decimals = 2;
        // all currency values are *100 to avoid decimals in database
        let value = Number(amount) / 100;
        if (value < 0.01 && value > 0) {
            // http://stackoverflow.com/a/31002148
            let numberOfZerosAfterDecimal = (-Math.floor( Math.log(value) / Math.log(10) + 1));
            return accounting.formatMoney(value, currencySymbol + " ", numberOfZerosAfterDecimal + 1, ",", ".");
        }
        return accounting.formatMoney(value, " ", decimals, ",", ".");
    },
    formatVat(vat) {
        return _.isNumber(vat) ? (vat/100).toString() + '%' : '';
    },
    getEstimatedRefund() {
        const fee = calculateFee(this.totalVAT);
        return Number(this.totalVAT - fee);
    },
    getTotalNumber() {
        let number = 0;
        _.each(this.lines, (line) => {
            if (line.number) {
                number += Number(line.number);
            }
        });
        return number;
    },
    getTotalAmount() {
        return Number(this.totalValue);
    },
    getLineTotal() {
        return (Number(this.amount) * Number(this.number || 1));
    },
    getVatTotals() {
        let vat = {};
        if (this.lines) {
            _.each(this.lines, (line) => {
                if (!_.has(vat, line.vat)) {
                    vat[line.vat] = {
                        vat: line.vat,
                        amount: 0,
                        exVatAmount: 0,
                        vatAmount: 0
                    };
                }
                const numberOf = Number(line.number || 1);
                vat[line.vat].amount += (numberOf * Number(line.amount));
                vat[line.vat].exVatAmount += (numberOf * (Number(line.amount) - Number(line.vatAmount)));
                vat[line.vat].vatAmount += (numberOf * Number(line.vatAmount));
            });
        } else {
            const exVat = this.totalValue - this.totalVAT;
            vat['-'] = {
                vat: Math.round(100*100 * this.totalVAT / exVat),
                amount: this.totalValue,
                exVatAmount: exVat,
                vatAmount: this.totalVAT
            }
        }
        return _.values(vat);
    },
    getClaimer() {
        return this.claimName || this.claimId;
    },
    getClaim(type) {
        if (this.claims) {
            return _.find(this.claims, (claim) => {
                return claim.claimType === type;
            });
        }
    }
};
