import { templateHelpers} from './template-helpers';

const getTravellerDomain = function() {
    let travellerDomain = 'https://traveller.vatfree.com/';

    if (Meteor.settings.public && Meteor.settings.public.travellerDomain) {
        travellerDomain = Meteor.settings.public.travellerDomain;
    }

    return travellerDomain;
};

Template.template_default.helpers(templateHelpers);
Template.template_default.events({
    'click .receipt-refund-estimate, click .receipt-refund-claim-now'(e) {
        e.preventDefault();
        const vatClaim = _.find(this.claims, (claim) => {
            return claim.claimType === 'vat-refund';
        });
        if (!vatClaim) {
            window.open(getTravellerDomain() + 'p/' + FlowRouter.getParam('id') + '/' + FlowRouter.getParam('hash'));
        }
    }
});
