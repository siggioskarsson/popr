import { ReactiveVar } from 'meteor/reactive-var';
import QRCode from 'qrcode';

Template.vatfreeAppDownload.onCreated(function() {
    this.showCopyReceipt = new ReactiveVar();
});

Template.vatfreeAppDownload.onRendered(function() {
    this.autorun(() => {
        let id = FlowRouter.getParam('id');
        let hash = FlowRouter.getParam('hash');
        if (id && hash) {
            Tracker.afterFlush(() => {
                const qrCodeCanvas = this.$('.qr-code > canvas')[0];
                const poprUrl = document.location.origin + document.location.pathname;
                QRCode.toCanvas(qrCodeCanvas, `${poprUrl}`, {
                    margin: 2,
                    width: 200,
                    scale: 6,
                    errorCorrectionLevel: 'H',
                    mode: 'alphanumeric'
                }, (err) => {
                    if (err) {
                        alert(err);
                    }
                });
            });
        }
    });

    this.showCopyReceipt.set(!!FlowRouter.getQueryParam('fr'));
});

Template.vatfreeAppDownload.helpers({
    isClaimed() {
        if (this.claims) {
            return _.find(this.claims, (claim) => {
                return claim.claimType === 'vat-refund';
            });
        }
    },
    showCopyReceipt() {
        return Template.instance().showCopyReceipt.get();
    }
});
