FlowRouter.notFound = {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "notFound"});
    }
};

FlowRouter.route('/check-request', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "check_request"});
    }
});

FlowRouter.route('/:id/:hash', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "receipt"});
    }
});
