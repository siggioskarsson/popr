import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { templateHelpers} from './template-helpers';
import './main.html';

Template.receipt.onCreated(function () {
    this.loading = new ReactiveVar(true);
    this.receipt = new ReactiveVar();
});

Template.receipt.onRendered(function () {
    let id = FlowRouter.getParam('id');
    let hash = FlowRouter.getParam('hash');

    if (id && hash) {
        const logAccess = !FlowRouter.getQueryParam('doNotLog');
        Meteor.call('get-receipt-details', id, hash, logAccess, (err, receipt) => {
            if (err) {
                alert(err.reason);
            } else {
                this.receipt.set(receipt);
            }
            this.loading.set(false);
        });
    } else {
        this.loading.set(false);
    }
});

Template.receipt.helpers(templateHelpers);
Template.receipt.helpers({
    isRetailer(retailerId) {
        return this.retailerId === retailerId;
    }
});
