import { sha256, receiptHash } from "meteor/popr:util/hash";
import hashSource from 'meteor/popr:util/hash-source'

Template.check_request.onCreated(function() {
    this.output = new ReactiveVar();
    this.errors = new ReactiveVar();
});

Template.check_request.helpers({
    getErrors() {
        return Template.instance().errors.get();
    },
    getOutput() {
        return Template.instance().output.get();
    }
});

Template.check_request.events({
    'submit form[name="json-request"]'(e, template) {
        e.preventDefault();
        template.output.set();
        template.errors.set();

        const output = [];
        const errors = [];
        let json;
        try {
            json = JSON.parse(template.$('textarea[name="jsonValue"]').val());
            let secret = template.$('input[name="secret"]').val();

            if (!json.time) {
                errors.push(`Missing required field "time"`);
            }
            if (!json.retailerId) {
                errors.push(`Missing required field "retailerId"`);
            }
            if (!json.invoiceId) {
                errors.push(`Missing required field "invoiceId"`);
            }
            if (!json.totalValue) {
                errors.push(`Missing required field "totalValue"`);
            }
            if (!json.totalVAT) {
                errors.push(`Missing required field "totalVAT"`);
            }
            if (!json.hash) {
                errors.push(`Missing required field "hash"`);
            }

            if (!errors.length) {
                const hashSourceString = hashSource(json);
                output.push('hashSource should be: ' + hashSourceString);

                const receiptHashString = receiptHash(json);
                output.push('hash should be: ' + receiptHashString);
                if (receiptHashString !== json.hash) {
                    errors.push(`Incorrect hashSource, should be: ${receiptHashString}`);
                }

                if (secret) {
                    // check hash
                    const hashSignature = sha256(json.hash + secret);
                    output.push('hashSignature should be: ' + hashSignature);
                    if (hashSignature !== json.hashSignature) {
                        errors.push(`Incorrect hashSignature, should be: ${hashSignature}`);
                    }
                }
            }
        } catch (e) {
            console.log(e);
            template.errors.set([e.message]);
            return false;
        }

        if (json && !errors.length) {
            Meteor.call('check-receipt-schema', json, (err, result) => {
                if (err) {
                    let errors = template.errors.get();
                    errors.push(err.message);
                    template.errors.set(errors);
                }
            });
        }

        template.output.set(output);
        template.errors.set(errors);
    }
});
