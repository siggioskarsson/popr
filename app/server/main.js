import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';
import { ScanReceiptLog } from 'meteor/popr:receipts/server/schemas/scan-receipt-log';

Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.methods({
    'get-receipt-details'(poprId, poprHash, logAccess = true) {
        let poprUrl = Meteor.settings.poprUrl || 'http://api.popr.io/';
        let url = poprUrl + 'invoice/' + poprId + '/' + poprHash;

        let poprReceipt;
        try {
            poprReceipt = HTTP.get(url);
        } catch(e) {
            console.log(e);
            throw new Meteor.Error(404, "We could not find the qr code in our database. Try again or register the receipt manually.");
        }

        let receipt = poprReceipt.data.result.invoice;

        let retailer = Retailers.findOne({_id: receipt.retailerId}) || {};
        receipt.logoImg = retailer.logoImg;
        receipt.retailerOptions = retailer.publicOptions || {};

        // log access
        const connection = this.connection;
        Meteor.defer(() => {
            if (logAccess) {
                ScanReceiptLog.insert({
                    poprId,
                    retailerId: receipt.retailerId,
                    shopId: receipt.shopId,
                    date: new Date(),
                    ipAddress: connection.clientAddress,
                    httpHeaders: connection.httpHeaders
                });
            }
        });

        return receipt;
    }
});
