import { Random } from "meteor/random";
import { sha256 } from "meteor/popr:util/hash";

Meteor.methods({
    'check-receipt-schema'(receipt) {
        check(receipt, Object);

        receipt = Receipts.simpleSchema().clean(receipt);
        receipt.createdAt = new Date();
        receipt.receiptKeyHash = sha256(Random.secret(64));
        return Receipts.simpleSchema().validate(receipt);
    }
});
