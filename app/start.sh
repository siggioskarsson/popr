#!/usr/bin/env bash
meteor npm install --no-optional
MONGO_URL=mongodb://localhost:8002/meteor meteor --port 8011 --settings settings.json
