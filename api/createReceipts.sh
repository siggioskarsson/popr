#!/bin/bash

PLATFORM='Linux'
SHAFUNC='sha256sum'
UNAMESTR=`uname`
if [[ "$UNAMESTR" == 'Darwin' ]]; then
   PLATFORM='osx'
   SHAFUNC='shasum -t -a 256'
fi

# RetailerId is the ID that was supplied by POPr
echo "Fill in the retailerId, followed by [ENTER]:"
read -s RETAILERID

# Retailer secret is a 64 cahracter hex string that will be used to "sign" the transactions
echo "Fill in the secret, followed by [ENTER]:"
read -s RETAILERSECRET

# In this example we do not have any NCP data (Name / Country / Passport) of the traveller
NCP_NAME=""
NCP_COUNTRY=""
NCP_PASSPORTNR=""
NCP_PASSPORTEXPIRATION=""

for i in {1..1}; do
  # create random data
  TIME=`date +"%s"`
  INVOICEID="VFRMDEy-${i}"
  VALUE=`echo $RANDOM % 100000 + 1 | bc`
  VAT=`printf %.0f $(echo "($VALUE - ($VALUE / 1.21))" | bc -l)`
  SHOPID="1"
  TERMINALID="ams2345"

  # hash source is calculated from "time/retailerId/shopId/terminalId/invoiceId/totalValue/totalVAT/ncp.name/ncp.country/ncp.passportNr/ncp.passportExpiration"
  HASHSOURCE="${TIME}/${RETAILERID}/${SHOPID}/${TERMINALID}/${INVOICEID}/${VALUE}/${VAT}/${NCP_NAME}/${NCP_COUNTRY}/${NCP_PASSPORTNR}/${NCP_PASSPORTEXPIRATION}"
  echo "HASHSOURCE = ${HASHSOURCE}"

  # Create a hash of the receipt data
  # online sha generator for testing: https://passwordsgenerator.net/sha256-hash-generator/
  SHA=`echo -n ${HASHSOURCE} | ${SHAFUNC} | cut -d ' ' -f 1`
  echo "SHA -> $SHA"

  # "sign" the hash with the retailer secret
  # online sha generator for testing: https://passwordsgenerator.net/sha256-hash-generator/
  SHASIG=`echo -n ${SHA}${RETAILERSECRET} | ${SHAFUNC} | cut -d ' ' -f 1`
  echo "SHASIG -> $SHASIG"

  # create the JSON data to send to the server
  DATA="{\"time\":${TIME},\"retailerId\":\"${RETAILERID}\",\"shopId\":\"${SHOPID}\",\"terminalId\":\"${TERMINALID}\",\"invoiceId\":\"${INVOICEID}\",\"totalValue\":${VALUE},\"totalVAT\":${VAT},\"hash\":\"${SHA}\",\"signature\":\"${SHASIG}\"}"
  echo "DATA -> $DATA"

  #echo "curl -H \"Content-Type: application/json\" -X POST -d \"${DATA}\" https://api.popr.io/invoice;";
  #curl -H "Content-Type: application/json" -X POST -d "${DATA}" https://api.popr.io/invoice;

  #echo "curl -H \"Content-Type: application/json\" -X POST -d \"${DATA}\" https://test.popr.io/invoice;";
  #curl -H "Content-Type: application/json" -X POST -d "${DATA}" https://test.popr.io/invoice;

  echo "curl -H \"Content-Type: application/json\" -X POST -d \"${DATA}\" http://localhost:8001/invoice;";
  curl -H "Content-Type: application/json" -X POST -d "${DATA}" http://localhost:8001/invoice;
done
