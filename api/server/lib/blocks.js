/*
import moment from 'moment';
import '../lib/receipts.js';

// https://www.npmjs.com/package/merkle-lib
var merkle = require('merkle-lib');
var fastRoot = require('merkle-lib/fastRoot');
var crypto = require('crypto');
var sha256 = function (data) {
    return crypto.createHash('sha256').update(data).digest();
};

Meteor.startup(() => {
    let block = Blocks.findOne();

    let data = block.receiptIds.map(x => new Buffer(x, 'utf-8'));
    var tree = merkle(data, sha256);
    console.log(tree.map(x => x.toString('hex')));

    var root = fastRoot(data, sha256);
    console.log(root.toString('hex'));
});


Blocks = new Meteor.Collection('blocks');
Blocks.attachSchema(new SimpleSchema({
    receiptIds: {
        type: [String],
        label: "All the receipt id's in this block",
        optional: false
    },
    previousBlock: {
        type: String,
        label: "Previous block",
        optional: true
    },
    merkleRoot: {
        type: String,
        label: "The merkle root of the receiptIds",
        optional: true
    },
    blockHash: {
        type: String,
        label: "The hash of this block to use to link to the blockchain - hash: sha(256(previousBlock, previousBlockHash, merkleRoot)",
        optional: true,
        index: 1
    }
}));

// Allow and deny rules
Blocks.allow({
    insert: function (userId, doc) {
        return true;
    },
    update: function (userId, doc, fields, modifier) {
        return false;
    },
    remove: function (userId, doc) {
        // We don't do deletes, only soft deletes
        return false;
    }
});

if (Meteor.isServer) {
    var processBlocks = function() {
        let currentBlockId = moment().format('YYYYMMDDHHmm');
        Blocks.find({
            _id: {
                $ne: currentBlockId
            },
            blockHash: {
                $exists: false
            }
        },{
            sort: {
                _id: 1
            }
        }).forEach((block) => {
            console.log('block', block);
            let previousBlock = Blocks.findOne({
                _id: {
                    $lt: block._id
                }
            },{
                sort: {
                    _id: -1
                }
            });
            if (!previousBlock) {
                if (Blocks.find().count() === 0) {
                    // first block
                    previousBlock = {};
                    previousBlock.blockHash = "0000000000000000000000000000000000000000000000000000000000000000";
                } else {
                    throw new Meteor.Error(500, 'Could not find previous block');
                }
            }
            console.log('previousBlock', previousBlock);

            let data = block.receiptIds.map(x => new Buffer(x, 'utf-8'));
            let root = fastRoot(data, sha256);
            let merkleRoot = root.toString('hex');
            Blocks.update({
                _id: block._id
            },{
                $set: {
                    previousBlock: block._id,
                    merkleRoot: merkleRoot,
                    blockHash: sha256(previousBlock._id + previousBlock.blockHash + merkleRoot)
                }
            });
        });
    };

    Blocks.after.insert(function(userId, doc) {
        console.log('Blocks.after.insert', userId, doc);
        let receiptId = doc.receiptIds[0];
        if (receiptId) {
            Receipts.update({
                _id: receiptId
            },{
                $set: {
                    blockId: doc._id
                }
            });
        }

        processBlocks();
    });

    Blocks.after.update(function(userId, doc, fieldNames, modifier, options) {
        console.log('Blocks.after.update', userId, doc, fieldNames, modifier, options);
        let receiptId = modifier && modifier['$addToSet'] && modifier['$addToSet'].receiptIds ? modifier['$addToSet'].receiptIds : false;
        if (receiptId) {
            Receipts.update({
                _id: receiptId
            },{
                $set: {
                    blockId: doc._id
                }
            });
        }
    });
}
*/