import { ScanReceiptLog } from 'meteor/popr:receipts/server/schemas/scan-receipt-log';

Meteor.methods({
    'update-stats'(secret) {
        if (secret !== Meteor.settings.adminSecret) {
            throw new Meteor.Error(404, 'Access denied');
        }

        Meteor.defer(() => {
            ReceiptStats.remove({});
            Receipts.find({}).forEach(function(receipt) {
                ReceiptStats.updateReceiptStats(receipt);
            });
        });
    },
    getStats(secret, p, r) {
        if (secret !== Meteor.settings.adminSecret) {
            throw new Meteor.Error(404, 'Access denied');
        }

        return ReceiptStats.find({
            p: p,
            r: r
        }).fetch();
    },
    'update-popr-scan-stats'(secret) {
        if (secret !== Meteor.settings.adminSecret) {
            throw new Meteor.Error(404, 'Access denied');
        }

        ScanReceiptLog.find({
            retailerId: {
                $exists: false
            }
        },{
            fields: {
                _id: 1,
                poprId: 1
            }
        }).forEach((s) => {
            const receipt = Receipts.findOne({_id: s.poprId}, {
                fields: {
                    retailerId: 1,
                    shopId: 1
                }
            });
            if (receipt) {
                ScanReceiptLog.update({
                    _id: s._id
                },{
                    $set: {
                        retailerId: receipt.retailerId,
                        shopId: receipt.shopId
                    }
                });
            }
        });
    }
});
