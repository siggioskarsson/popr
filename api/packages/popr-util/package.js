Package.describe({
    name: 'popr:util',
    summary: 'POPr utils package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'ecmascript',
        'check',
        'underscore'
    ]);

    // shared files
    api.addFiles([
        'hash.js',
        'hash-source.js'
    ]);

    // server files
    api.addFiles([
    ], 'server');

    // client files
    api.addFiles([
    ], 'client');

    api.export([
    ], 'server'); // temp, only on the server
});

Package.onTest(function(api) {
    api.use([
        'popr:util'
    ]);

    api.use([
        'ecmascript',
        'check',
        'underscore',
        'meteortesting:mocha',
        'practicalmeteor:sinon',
        'practicalmeteor:chai'
    ]);

    api.addAssets([
    ], 'server');

    api.addFiles([
        'tests/hash.js',
        'tests/hash-source.js'
    ], 'server');

    api.export([
    ]);
});
