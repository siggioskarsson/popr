import { sha256, receiptHash } from "../hash";

describe('hashes', function () {
  it('sha256 hashing', function () {
      const hashString = "1489505590/tkmWjDednjSfcWSgB/234346/ams2345-33/VFRMDEyMzQ2Nzg5MDEyMzQ2Nzg5/21995/3818////";
      const hash = "06afac4101c81fb18726ee1145862c0453dcc0e0bbe96e6ede42a4328082b364"; // echo -n "${hashString}" | openssl dgst -sha256
      expect(hash).to.equal(sha256(hashString));
  });

  it('receipt hashing', function () {
      const receipt = {
          "time": 1489505590,
          "retailerId": "tkmWjDednjSfcWSgB",
          "shopId": "234346",
          "terminalId": "ams2345-33",
          "invoiceId": "VFRMDEyMzQ2Nzg5MDEyMzQ2Nzg5",
          "totalValue": 21995,
          "totalVAT": 3818,
          "hash": "039b003601e6a9d0...2e284826fc8f0328356afff0934409fe84e",
          "signature": "93d46e09851efeee52ef779e8...986ad03ece3d29491d9bdaffe"
      };
      const hash = "06afac4101c81fb18726ee1145862c0453dcc0e0bbe96e6ede42a4328082b364"; // echo -n "${hashString}" | openssl dgst -sha256
      expect(hash).to.equal(receiptHash(receipt));
  });
});
