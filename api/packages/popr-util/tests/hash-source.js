import hashSource from '../hash-source';

describe('hash source tests', function () {
    it('simple receipt', function () {
        const receipt = {
            "time": 1489505590,
            "retailerId": "tkmWjDednjSfcWSgB",
            "shopId": "234346",
            "terminalId": "ams2345-33",
            "invoiceId": "VFRMDEyMzQ2Nzg5MDEyMzQ2Nzg5",
            "totalValue": 21995,
            "totalVAT": 3818,
            "hash": "039b003601e6a9d0...2e284826fc8f0328356afff0934409fe84e",
            "signature": "93d46e09851efeee52ef779e8...986ad03ece3d29491d9bdaffe"
        };

        const hashString = "1489505590/tkmWjDednjSfcWSgB/234346/ams2345-33/VFRMDEyMzQ2Nzg5MDEyMzQ2Nzg5/21995/3818////";
        expect(hashString).to.equal(hashSource(receipt));
    });

    it('simple receipt with slashes', function () {
        const receipt = {
            "time": 1489505590,
            "retailerId": "tkmWjDednjSfcWSgB",
            "shopId": "234/346",
            "terminalId": "ams/2345-33",
            "invoiceId": "VFRMDEyMzQ2N/zg5MDEyMzQ2Nzg5",
            "totalValue": 21995,
            "totalVAT": 3818,
            "hash": "039b003601e6a9d0...2e284826fc8f0328356afff0934409fe84e",
            "signature": "93d46e09851efeee52ef779e8...986ad03ece3d29491d9bdaffe"
        };

        const hashString = "1489505590/tkmWjDednjSfcWSgB/234/346/ams/2345-33/VFRMDEyMzQ2N/zg5MDEyMzQ2Nzg5/21995/3818////";
        expect(hashString).to.equal(hashSource(receipt));
    });

    it('ncp receipt', function () {
        const receipt = {
            "_id": "NPM9SABQhG5PQmTEt",
            "time": 1520856659,
            "retailerId": "tkmWjDednjSfcWSgB",
            "shopId": "1",
            "terminalId": "ams2345-33",
            "invoiceId": "WadkoR2pbwP4qe34F",
            "totalValue": 21995,
            "totalVAT": 3818,
            "vatLines": [
                {
                    "vat": 2100,
                    "vatAmount": 3818
                },
                {
                    "vat": 600,
                    "vatAmount": 0
                }
            ],
            "ncp": {
                "name": "Doe, John Wilkens",
                "country": "US",
                "passportNr": "AV2342535",
                "passportExpiration": "2018-01-01"
            },
            "hash": "232f33559c5dd09020ac30c69d7a66bc49cac3a18d3db7aa4bb5207e93ae4a38",
            "receiptKeyHash": "7550f9ca0472d100a1d9fba5ead23540cbe7ba4f8ce04f1f02950e14e85738fd",
            "createdAt": new Date(1520856968629)
        };

        const hashString = "1520856659/tkmWjDednjSfcWSgB/1/ams2345-33/WadkoR2pbwP4qe34F/21995/3818/Doe, John Wilkens/US/AV2342535/2018-01-01";
        expect(hashString).to.equal(hashSource(receipt));
    });
});
