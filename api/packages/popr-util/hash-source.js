/* eslint prefer-template: "off" */

const hashSource = function(receipt) {
  const ncp = receipt.ncp || {};
  return receipt.time.toString() + '/' +
    receipt.retailerId + '/' +
    (receipt.shopId || '') + '/' +
    (receipt.terminalId || '') + '/' +
    receipt.invoiceId + '/' +
    receipt.totalValue.toString() + '/' +
    receipt.totalVAT.toString() + '/' +
    (ncp.name || '') + '/' +
    (ncp.country || '') + '/' +
    (ncp.passportNr || '') + '/' +
    (ncp.passportExpiration || '')
    ;
};

export default hashSource;
