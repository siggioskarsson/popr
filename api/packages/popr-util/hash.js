const crypto = require('crypto');
import hashSource from './hash-source.js'

const sha256 = function(string) {
  const c = crypto.createHash('sha256');
  c.update(new Buffer(string, 'utf8'));

  const buf = c.digest();
  return buf.toString('hex');
};

/**
 * Create the POPr receipt hashes from the the receipt object
 * @param receipt
 */
const receiptHash = function(receipt) {
  return sha256(hashSource(receipt));
};

export { sha256 as sha256 };
export { receiptHash as receiptHash};
