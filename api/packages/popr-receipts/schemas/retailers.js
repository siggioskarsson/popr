Retailers = new Meteor.Collection('retailers');
Retailers.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Name of the retailer",
        optional: false
    },
    logoImg: {
        type: String,
        label: "logo of this retailer",
        optional: true
    },
    publicOptions: {
        type: Object,
        label: "Rendering options on the public site. Viewable by everyone",
        blackbox: true,
        optional: true
    },
    hashSecret: {
        type: String,
        label: "Hash secret",
        optional: false
    },
    publicKey: {
        type: String,
        label: "Public key for signing invoice requests",
        optional: true
    }
}));

// Allow and deny rules
Retailers.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fields, modifier) {
        return false;
    },
    remove: function (userId, doc) {
        // We don't do deletes, only soft deletes
        return false;
    }
});
