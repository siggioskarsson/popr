const VatLinesSchema = new SimpleSchema({
    vat: {
        type: Number,
        label: "Vat percentage *100 in vatLines attribute",
        optional: false
    },
    vatAmount: {
        type: Number,
        label: "The actual vat amount *100 in vatLines attribute",
        optional: false
    }
});
const InvoiceLinesSchema = new SimpleSchema({
    productId: {
        type: String,
        label: "Unique id of the product in lines attribute",
        optional: false
    },
    description: {
        type: String,
        label: "Descriptive of the product in lines attribute",
        optional: false
    },
    number: {
        type: Number,
        label: "The number of items of the product in lines attribute",
        optional: false,
        defaultValue: 1
    },
    amount: {
        type: Number,
        label: "Total price of product in lines attribute",
        optional: false
    },
    vat: {
        type: Number,
        label: "VAT rate of this product in lines attribute",
        optional: false,
        defaultValue: 0
    },
    discount: {
        type: Number,
        label: "Discount given on this line item",
        optional: true
    },
    vatAmount: {
        type: Number,
        label: "Total vat amount of this product in lines attribute",
        optional: false,
        defaultValue: 0
    }
}, {
    clean: {
        removeEmptyStrings: false
    }
});

const FileSchema = new SimpleSchema({
    name: {
        type: String,
        label: "Name of the file",
        optional: false,
    },
    type: {
        type: String,
        label: "Mime type of the file",
        optional: false,
    },
    size: {
        type: Number,
        label: "Size of the file in bytes",
        optional: false,
    },
    url: {
        type: String,
        label: "Url of the file, POPr does not save files itself",
        optional: false,
    },
    hash: {
        type: String,
        label: "Hash of the file",
        optional: false
    }
});

Receipts = new Meteor.Collection('receipts');
Receipts.attachSchema(new SimpleSchema({
    time: {
        type: Number,
        label: "Time this receipt was created in the shop",
        optional: false
    },
    createdAt: {
        type: Date,
        label: "The date this receipt was added to our database",
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: new Date()
                };
            } else {
                this.unset();
            }
        },
        optional: false
    },
    retailerId: {
        type: String,
        label: "Internal ID of the retailer",
        optional: false
    },
    shopId: {
        type: String,
        label: "Internal ID of the shop",
        optional: true
    },
    terminalId: {
        type: String,
        label: "Internal ID of the terminal",
        optional: true
    },
    employeeId: {
        type: String,
        label: "Internal ID of the shop employee",
        optional: true
    },
    invoiceId: {
        type: String,
        label: "Internal ID of the invoice",
        optional: false
    },
    customerId: {
        type: String,
        label: "Internal ID of the customer",
        optional: true
    },
    totalValue: {
        type: Number,
        label: "Total value of this receipt",
        optional: false
    },
    totalVAT: {
        type: Number,
        label: "Total value of this receipt",
        optional: false,
        defaultValue: 0
    },
    currency: {
        type: String,
        label: "ISO 4217, 3 letter currency code",
        optional: true
    },
    paymentType: {
        type: String,
        label: "Payment type used to pay for the purchase",
        optional: true
    },
    message: {
        type: String,
        label: "Message to display at the bottom of a receipt",
        optional: true
    },
    vatLines: {
        type: [VatLinesSchema],
        label: "The VAT lines of the invoice",
        optional: true
    },
    lines: {
        type: [InvoiceLinesSchema],
        label: "The detail of the invoice",
        optional: true
    },
    ncp: {
        type: String,
        label: "Encrypted version of Name/Country/Passport schema, uses the receipt key",
        optional: true
    },
    tags: {
        type: [String],
        label: "Optional free-form tags that can be added to the receipt on creation",
        optional: true
    },
    files: {
        type: [FileSchema],
        label: "Any files linked to this receipt",
        optional: true
    },
    claims: {
        type: [Receipts.claimSchema],
        label: "Claims that have been made to this invoice",
        optional: true,
        blackbox: true
    },
    hash: {
        type: String,
        label: "Hash of the receipt",
        optional: false
    },
    receiptKeyHash: {
        type: String,
        label: "Hash of the receipt key",
        optional: false
    },
    hashSignature: {
        type: String,
        label: "Hash of the hash and secret of the retailer of the receipt",
        optional: true
    },
    signature: {
        type: String,
        label: "Signature for the hash of the receipt for the retailer",
        optional: true
    },
    blockId: {
        type: String,
        label: "The block this receipt was 'mined' into",
        optional: true
    },
    revokedAt: {
        type: Date,
        label: "When the receipt was revoked by the retailer",
        optional: true
    },
    revokedBy: {
        type: String,
        label: "Who revoked the receipt",
        optional: true
    }
}));
Receipts.ncpSchema = new SimpleSchema({
    name: {
        type: String,
        label: "Name of the user claiming the receipt, passport format (lastname, names)",
        optional: false
    },
    country: {
        type: String,
        label: "Country 2 letter code",
        optional: false
    },
    passportNr: {
        type: String,
        label: "Passport Nr (string)",
        optional: false
    },
    passportExpiration: {
        type: String,
        label: "Date of passport expiration",
        optional: false
    }
});
Receipts.claimSchema = new SimpleSchema({
    claimType: {
        type: String,
        label: "Claim type",
        allowedValues: [
            'vat-refund'
        ],
        optional: false
    },
    claimId: {
        type: String,
        label: "External id of the claim",
        optional: false
    },
    claimName: {
        type: String,
        label: "External name of the claiming entity",
        optional: true
    },
    claimData: {
        type: Object,
        label: "Extra data to save with the claim",
        optional: true,
        blackbox: true
    },
    claimedAt: {
        type: Date,
        label: "Date the claim was made",
        optional: false
    },
    claimTokenHash: {
        type: String,
        label: "Claim token hash to manipulate the claim",
        optional: false
    }
});

// Allow and deny rules
Receipts.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fields, modifier) {
        return false;
    },
    remove: function (userId, doc) {
        // We don't do deletes, only soft deletes
        return false;
    }
});
