import moment from 'moment';

ReceiptStats = new Meteor.Collection('receipt-stats');
ReceiptStats.attachSchema(new SimpleSchema({
    p: {
        type: String,
        label: "In which period these stats are from",
        allowedValues: [
            'w',
            'm',
            'q',
            'y'
        ],
        optional: false
    },
    pv: {
        type: Number,
        label: "Value for period (w: 201801, m: 201801, q: 20181, y: 2018)",
        optional: false
    },
    r: {
        type: String,
        label: "Retailer ID",
        optional: false
    },
    c: {
        type: Number,
        label: "Number of receipts in period",
        optional: true
    },
    a: {
        type: Number,
        label: "Purchase amount of receipts in period",
        optional: true
    },
    v: {
        type: Number,
        label: "VAT amount of receipts in period",
        optional: true
    }
}));

ReceiptStats._ensureIndex({p: 1, r: 1});

ReceiptStats._updateStats = function (p, pv, r, doc) {
    ReceiptStats.upsert({
        p: p,
        pv: pv,
        r: r
    }, {
        $inc: {
            c: 1,
            a: doc.totalValue,
            v: doc.totalVAT
        }
    });
};
ReceiptStats.updateReceiptStats = function (doc) {
    let mDate = moment.unix(doc.time);
    let periods = {
        w: Number(mDate.format('GGGGWW')),
        m: Number(mDate.format('YYYYMM')),
        q: Number(mDate.format('YYYYQ')),
        y: Number(mDate.format('YYYY'))
    };
    _.each(periods, (pv, p) => {
        ReceiptStats._updateStats(p, pv, 'A', doc);
        ReceiptStats._updateStats(p, pv, doc.retailerId, doc);
    });
};

Receipts.after.insert(function(userId, doc) {
    // update stats for all relevant stuff
    Meteor.defer(function() {
        ReceiptStats.updateReceiptStats(doc);
    });
});
