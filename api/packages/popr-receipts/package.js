Package.describe({
    name: 'popr:receipts',
    summary: 'POPr receipts package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'matb33:collection-hooks@0.8.4',
        'meteorhacks:unblock@1.1.0'
    ]);

    // shared files
    api.addFiles([
    ]);

    // server files
    api.addFiles([
        'schemas/receipts.js', // this is temp, receipts schema is only on the server
        'schemas/retailers.js',
        'server/schemas/receipt-stats.js',
        'server/schemas/receipts.js'
    ], 'server');

    // client files
    api.addFiles([
    ], 'client');

    api.export([
        'Receipts',
        'Retailers',
        'ReceiptStats'
    ], 'server'); // temp, only on the server
});
