import { ScanReceiptLog } from 'meteor/popr:receipts/server/schemas/scan-receipt-log';

export const getStats = function() {
    try {
        const params = JSON.parse(JSON.stringify(this.bodyParams));
        const period = this.urlParams.period;
        const retailerId = this.urlParams.retailerId;

        if (!params.secret) {
            return statusCodes.noAccess;
        }
        if (params.secret !== Meteor.settings.adminSecret) {
            return statusCodes.noAccess;
        }

        const selector = {
            p: period,
            r: retailerId
        };

        if (params.pvFrom) {
            if (!selector.pv) selector.pv = {};
            selector.pv.$gte = params.pvFrom;
        }
        if (params.pvTill) {
            if (!selector.pv) selector.pv = {};
            selector.pv.$lte = params.pvTill;
        }

        return ReceiptStats.find(selector, {
            fields: {
                _id: false,
                p: false,
                r: false
            },
            limit: 520
        }).fetch();
    } catch(e) {
        console.log(e);
        return statusCodes.processingError;
    }
};

export const getStatsPerShop = function() {
    try {
        const params = JSON.parse(JSON.stringify(this.bodyParams));
        if (!params.secret) {
            return statusCodes.noAccess;
        }
        if (params.secret !== Meteor.settings.adminSecret) {
            return statusCodes.noAccess;
        }

        let fromDate = this.urlParams.fromDate;
        let tillDate = this.urlParams.tillDate;
        const retailerId = this.urlParams.retailerId;

        let selector = {};
        if (retailerId && retailerId !== '0') {
            selector.retailerId = retailerId;
        }
        if (fromDate && fromDate !== '0') {
            selector.time = {
                $gte: Number(fromDate)
            }
        }
        if (tillDate && tillDate !== '0') {
            if (selector.time) {
                selector.time.$lte = Number(tillDate);
            } else {
                selector.time = {
                    $lte: Number(tillDate)
                }
            }
        }

        const pipeline = [
            {
                "$match": selector
            },
            {
                "$group": {
                    "_id": {
                        $concat: ["$retailerId", "/", "$shopId"]
                    },
                    "c": {
                        "$sum": 1
                    },
                    "a": {
                        "$sum": "$totalValue"
                    },
                    "v": {
                        "$sum": "$totalVAT"
                    }
                }
            }
        ];

        const _aggregate = function(callback) {
            Receipts.rawCollection().aggregate(pipeline, {
                readPreference: 'secondaryPreferred'
            }).toArray(callback);
        };
        const aggregate = Meteor.wrapAsync(_aggregate);

        return aggregate();
    } catch(e) {
        console.log(e);
        return statusCodes.processingError;
    }
};

export const getScanStats = function() {
    try {
        const params = JSON.parse(JSON.stringify(this.bodyParams));
        if (!params.secret) {
            return statusCodes.noAccess;
        }
        if (params.secret !== Meteor.settings.adminSecret) {
            return statusCodes.noAccess;
        }

        const fromDate = this.urlParams.fromDate;
        const tillDate = this.urlParams.tillDate;
        let aggr = this.urlParams.aggregate;
        // default month aggregation
        let aggregationKey = {
            $toInt: {
                $concat: [
                    {
                        $toString: {
                            $isoWeekYear: '$date'
                        }
                    }, {
                        $cond: [
                            {
                                $lte: [ { $isoWeek: "$date" }, 9 ] },
                            {
                                $concat: [
                                    "0", { $toString: { $isoWeek: "$date" } }
                                ]
                            },
                            {
                                $substr: [ { $isoWeek: "$date" }, 0, 2 ]
                            }
                        ]
                    },
                ]
            }
        };
        if (aggr === 'shop') {
            aggregationKey = {
                $concat: ['$retailerId', '/', '$shopId']
            };
        } else if (aggr === 'retailer') {
            aggregationKey = '$retailerId';
        } else if (aggr !== 'week') {
            // month aggregation
            aggregationKey = {
                $toInt: {
                    $concat: [
                        {
                            $toString: {
                                $year: '$date'
                            }
                        }, {
                            $cond: [
                                {
                                    $lte: [ { $month: "$date" }, 9 ] },
                                {
                                    $concat: [
                                        "0", { $toString: { $month: "$date" } }
                                    ]
                                },
                                {
                                    $substr: [ { $month: "$date" }, 0, 2 ]
                                }
                            ]
                        },
                    ]
                }
            };
        }

        let selector = {};
        if (fromDate && fromDate !== '0') {
            selector.time = {
                $gte: Number(fromDate)
            }
        }
        if (tillDate && tillDate !== '0') {
            if (selector.time) {
                selector.time.$lte = Number(tillDate);
            } else {
                selector.time = {
                    $lte: Number(tillDate)
                }
            }
        }

        const pipeline = [
            {
                '$match': {
                    'poprId': {
                        '$exists': true
                    }
                }
            }, {
                '$group': {
                    '_id': {
                        'poprId': '$poprId',
                        'aggr': aggregationKey
                    },
                    'count': {'$sum': 1}
                }
            }, {
                '$group': {
                    '_id': {
                        'aggr': '$_id.aggr'
                    },
                    'totalCount': {
                        '$sum': '$count'
                    },
                    'distinctCount': {
                        '$sum': 1
                    }
                }
            }
        ];

        const _aggregate = function(callback) {
            ScanReceiptLog.rawCollection().aggregate(pipeline, {
                readPreference: 'secondaryPreferred'
            }).toArray(callback);
        };
        const aggregate = Meteor.wrapAsync(_aggregate);

        return aggregate();
    } catch(e) {
        console.log(e);
        return statusCodes.processingError;
    }
};
