export const getRetailers = function() {
    try {
        const params = JSON.parse(JSON.stringify(this.bodyParams));
        if (!params.secret) {
            return statusCodes.noAccess;
        }
        if (params.secret !== Meteor.settings.adminSecret) {
            return statusCodes.noAccess;
        }

        return Retailers.find({}, {
            fields: {
                id: true,
                name: true
            }
        }).fetch();
    } catch(e) {
        console.log(e);
        return statusCodes.processingError;
    }
};
