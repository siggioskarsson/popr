const crypto = require('crypto');

var _encrypt = function (string, password, callback) {
    const cipher = crypto.createCipher('aes256', password);

    let encrypted = cipher.update(string, 'utf8', 'hex');
    encrypted += cipher.final('hex');

    callback(null, encrypted);
};
export var encrypt = Meteor.wrapAsync(_encrypt);
var _decrypt = function (encrypted, password, callback) {
    const decipher = crypto.createDecipher('aes256', password);
    let decrypted = decipher.update(encrypted, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    callback(null, decrypted);
};
export var decrypt = Meteor.wrapAsync(_decrypt);
