import { receiptHash, sha256 } from 'meteor/popr:util/hash';
import { decrypt, encrypt } from './encryption';
import moment from 'moment';

const _insertReceipt = function(receipt, callback) {
    Receipts.insert(receipt, callback);
};
const insertReceipt = Meteor.wrapAsync(_insertReceipt);

/**
 * curl -H "Content-Type: application/json" -X POST -d '{"time":1489505590,"retailerId":"tkmWjDednjSfcWSgB","shopId":"234346","invoiceId":"VFRMDEyMzQ2Nzg5MDEyMzQ2Nzg5","totalValue":21995,"totalVAT":3818,"hash":"faf3a1fd295fa995e7dac75bbeaf321f547714ea0e71a0cb2dda080b4fa22178"}' http://localhost:8001/invoice
 * curl -H "Content-Type: application/json" -X POST -d '{"time":1489505590,"retailerId":"tkmWjDednjSfcWSgB","shopId":"234346","terminalId":"ams2345-33","invoiceId":"VFRMDEyMzQ2Nzg5MDEyMzQ2Nzg5","totalValue"21995,"totalVAT":3818,"hash":"06afac4101c81fb18726ee1145862c0453dcc0e0bbe96e6ede42a4328082b364"}' http://localhost:8001/invoice
 * curl -H "Content-Type: application/json" -X POST -d '{"time":1489505590,"retailerId":"tkmWjDednjSfcWSgB","shopId":"234346","terminalId":"ams2345-33","invoiceId":"VFRMDEyMzQ2Nzg5MDEyMzQ2Nzg5","totalValue":21995,"totalVAT":3818,"ncp":{"name":"Doe, John Wilkens","country":"US","passportNr":"AV2342535","passportExpiration":"2018-01-01"},"hash":"67245c81e84e02d0c90f9bb2aebb66c403293ab92949f346f79969e4131fc10a"}' http://localhost:8001/invoice
 */
export const createInvoice = function () {
    try {
        let receipt = _.clone(this.bodyParams);
        if (!receipt.time || !receipt.retailerId || !receipt.invoiceId || !receipt.totalValue || !receipt.hash) {
            return statusCodes.incompleteRequest;
        }

        let invoiceHash = receiptHash(receipt);
        if (invoiceHash !== receipt.hash) {
            return statusCodes.hashError;
        }

        let retailer = Retailers.findOne({_id: receipt.retailerId});
        if (!retailer) {
            return statusCodes.retailerNotFound;
        }

        // Some systems have hex in uppercase
        receipt.hash = (receipt.hash || '').toLowerCase();
        receipt.hashSignature = (receipt.hashSignature || '').toLowerCase();

        if (receipt.hashSignature) {
            // check the retailer secret
            if (sha256(receipt.hash + retailer.hashSecret) !== receipt.hashSignature) {
                return statusCodes.hashSignatureInvalid;
            }
        }

        let receiptKey = Random.secret(64);
        receipt.receiptKeyHash = sha256(receiptKey);

        if (receipt.ncp) {
            if (receipt.ncp.passportExpiration && !moment(receipt.ncp.passportExpiration, 'YYYY-MM-DD').isValid()) {
                return statusCodes.ncpInvalid;
            }

            let valid = Receipts.ncpSchema.namedContext().validate(receipt.ncp, {modifier: false});
            if (valid) {
                receipt.ncp = encrypt(JSON.stringify(receipt.ncp), receiptKey);
            } else {
                return statusCodes.ncpInvalid;
            }
        }

        // TODO: validation does not work properly here ...
        //let valid = Receipts.simpleSchema().namedContext().validate(receipt, {modifier: false});
        let receiptId;
        //if (valid) {
        // receipt should be unique - we do not allow inserting exactly the same receipt
        if (Receipts.findOne({hash: receipt.hash}, {fields: {_id: 1}})) {
            return statusCodes.receiptExists;
        } else {
            delete receipt._id; // delete if set from external
            receiptId = insertReceipt(receipt);
        }
        //} else {
        //    return statusCodes.dataInvalid;
        //}

        if (receiptId) {
            return {
                statusCode: 200,
                body: {
                    status: 'ok',
                    message: 'ok',
                    result: {
                        id: receiptId + '/' + receiptKey
                    }
                }
            };
        } else {
            return statusCodes.processingError;
        }
    } catch (e) {
        console.log(e);
        return statusCodes.processingError;

    }
};
/**
 * curl -H "Content-Type: application/json" -X GET -d '{"id": "96C7wRBF2dqm8vu3p/lZP29ow9WHrtrED40ATkb4z-N5QLcDnGx7f9tH-qLm_wd6_eaUM4wjl1rkrXIyTn"}' http://localhost:8001/invoice
 * echo -n "lZP29ow9WHrtrED40ATkb4z-N5QLcDnGx7f9tH-qLm_wd6_eaUM4wjl1rkrXIyTn" | shasum -a 256 -t -
 */

export const getInvoice = function (checkReceiptKey = true) {
    try {
        let receiptId, receiptKey;
        if (this.urlParams && this.urlParams.id) {
            receiptId = this.urlParams.id;
            receiptKey = this.urlParams.key;
        } else {
            let params = _.clone(this.bodyParams);
            if (params && params.id) {
                let id = params.id.split('/');
                receiptId = id[0];
                receiptKey = id[1];
            }
        }

        if (!receiptId || (checkReceiptKey && !receiptKey)) {
            return statusCodes.incompleteRequest;
        }

        let receipt = Receipts.findOne({_id: receiptId});
        if (receipt && (!checkReceiptKey || sha256(receiptKey) === receipt.receiptKeyHash)) {
            delete receipt.receiptKeyHash;
            delete receipt.hashSignature;
            delete receipt.signature;
            if (receipt.ncp && checkReceiptKey) {
                receipt.ncp = JSON.parse(decrypt(receipt.ncp, receiptKey));
            }
            return {
                statusCode: 200,
                body: {
                    status: 'ok',
                    message: 'ok',
                    result: {
                        invoice: receipt
                    }
                }
            };
        } else {
            return statusCodes.incorrectReceiptKey;
        }
    } catch (e) {
        console.log(e);
        return statusCodes.processingError;
    }
};

/**
 * curl -H "Content-Type: application/json" -XPOST -d '{"id": "JY5FxhCxbXdjc7jvk/zt_KljPOuWKGYd3uBGay0BsXIQIDVqxuTVZ9h_LXlswoFbEIowuz3s6cEjY8A5Pn", "hashSignature": "84cebb7ded7877a19468722eb915d7d0f3f280575fdb7c184bb4e150faa0e075"}' http://localhost:8001/invoice/revoke
 */
export const revokeInvoice = function () {
    try {
        let receiptId, receiptKey, hashSignature;
        if (this.urlParams && this.urlParams.id) {
            receiptId = this.urlParams.id;
            receiptKey = this.urlParams.key;
            hashSignature = (this.urlParams.hashSignature || '').toLowerCase();
        } else {
            let params = _.clone(this.bodyParams);
            if (params && params.id) {
                let id = params.id.split('/');
                receiptId = id[0];
                receiptKey = id[1];
                hashSignature = (params.hashSignature || '').toLowerCase();
            }
        }
        console.log('revoke invoice', receiptId);

        if (!receiptId || !receiptKey) {
            return statusCodes.incompleteRequest;
        }

        let receipt = Receipts.findOne({_id: receiptId});
        if (receipt && sha256(receiptKey) === receipt.receiptKeyHash) {
            if (receipt.revokedAt) {
                return statusCodes.receiptRevoked;
            }

            let retailer = Retailers.findOne({_id: receipt.retailerId});
            if (!retailer) {
                return statusCodes.retailerNotFound;
            }

            if (receipt.hash) {
                // check the retailer secret
                if (sha256(receipt.hash + 'revoke' + retailer.hashSecret) !== hashSignature) {
                    return statusCodes.hashSignatureInvalid;
                }
            }

            Receipts.update({
                _id: receipt._id,
            }, {
                $set: {
                    revokedAt: new Date(),
                    revokedBy: this.request.headers['x-forwarded-for']
                }
            });

            return {
                statusCode: 200,
                body: {
                    status: 'ok',
                    message: 'ok',
                    result: {}
                }
            };
        } else {
            return statusCodes.incorrectReceiptKey;
        }
    } catch (e) {
        console.error(e);
        return statusCodes.processingError;
    }
};

export const getInvoices = function(selector) {
    try {
        return {
            statusCode: 200,
            body: {
                status: 'ok',
                message: 'ok',
                result: {
                    invoices: Receipts.find(selector, {
                        fields: {
                            receiptKeyHash: false,
                            hashSignature: false,
                            signature: false
                        }
                    }).fetch()
                }
            }
        };
    } catch(e) {
        console.error(e);
        return statusCodes.processingError;
    }
};

export const addFileToInvoice = function(checkReceiptKey = true) {
    try {
        const params = _.clone(this.bodyParams);
        let receiptId = params.id;
        let receiptKey;
        if (receiptId.indexOf('/' > 0)) {
            [receiptId, receiptKey] = receiptId.split('/');
        }

        if (!receiptId || (!receiptKey && checkReceiptKey)) {
            return statusCodes.incompleteRequest;
        }

        let receipt = Receipts.findOne({_id: receiptId});
        if (receipt && (!checkReceiptKey || sha256(receiptKey) === receipt.receiptKeyHash)) {
            Receipts.update({
                _id: receipt._id,
            }, {
                $addToSet: {
                    files: params.file
                }
            });

            return {
                statusCode: 200,
                body: {
                    status: 'ok',
                    message: 'ok',
                    result: {}
                }
            };
        } else {
            return statusCodes.incorrectReceiptKey;
        }
    } catch (e) {
        console.error(e);
        return statusCodes.processingError;
    }
};
