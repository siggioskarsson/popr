export const searchInvoices = function() {
    try {
        let searchTerm;
        if (this.urlParams && this.urlParams.searchTerm) {
            searchTerm = this.urlParams.searchTerm;
        } else {
            searchTerm = this.bodyParams.searchTerm;
        }

        const receipts = [];
        Receipts.find({
            _id: new RegExp('^' + searchTerm, "i")
        },{
            limit: 10
        }).forEach((receipt) => {
            receipts.push(receipt);
        });

        return {
            statusCode: 200,
            body: {
                status: 'ok',
                message: 'ok',
                result: receipts
            }
        };
    } catch (e) {
        console.log(e);
        return statusCodes.processingError;
    }
};
