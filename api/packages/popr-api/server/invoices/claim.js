import { sha256 } from 'meteor/popr:util/hash';

/**
 * curl -H "Content-Type: application/json" -XPOST -d '{"id": "JY5FxhCxbXdjc7jvk/zt_KljPOuWKGYd3uBGay0BsXIQIDVqxuTVZ9h_LXlswoFbEIowuz3s6cEjY8A5Pn", "claim": "vat-refund", "claimId": "vatfree:aLnNrRXaCw3DjXiDT"}' http://localhost:8001/invoice/claim
 */
export const claimInvoice = function (checkReceiptKey = true) {
    try {
        let receiptId, receiptKey, params;
        if (this.urlParams && this.urlParams.id) {
            receiptId = this.urlParams.id;
            receiptKey = this.urlParams.key;
            params = _.clone(this.urlParams);
        } else {
            params = _.clone(this.bodyParams);
            if (params && params.id) {
                let id = params.id.split('/');
                receiptId = id[0];
                receiptKey = id[1];
            }
        }

        if (!receiptId || (checkReceiptKey && !receiptKey)) {
            return statusCodes.incompleteRequest;
        }

        let receipt = Receipts.findOne({_id: receiptId});
        if (receipt && (!checkReceiptKey || sha256(receiptKey) === receipt.receiptKeyHash)) {
            let retailer = Retailers.findOne({_id: receipt.retailerId});
            if (!retailer) {
                return statusCodes.dataInvalid;
            }

            if (receipt.revokedAt || receipt.revokedBy) {
                return statusCodes.receiptRevoked;
            }

            let claimType = params.claim;
            let claimId = params.claimId;
            let claimName = params.claimName || null;
            let claimData = params.claimData || null;

            if (!claimType || !claimId) {
                return statusCodes.dataInvalid;
            }

            if (claimData) {
                // check claimData
                // only 1 dimensional data object with string keys is allowed
                // maximum of 32 keys
                _.each(claimData, (value, key) => {
                    if (typeof value !== 'string') {
                        return statusCodes.claimDataInvalid;
                    }
                });
                if (_.size(claimData) > 32) {
                    return statusCodes.claimDataInvalid;
                }
            }

            if (receipt.claims) {
                // check whether the same claim type has already been made
                let claim = _.find(receipt.claims, (receiptClaim) => {
                    return receiptClaim.claimType === claimType;
                });
                if (claim) {
                    return statusCodes.receiptAlreadyClaimed;
                } else {

                }
            }

            let claimToken = Random.secret();

            Receipts.update({
                _id: receipt._id,
            }, {
                $addToSet: {
                    claims: {
                        claimType: claimType,
                        claimId: claimId,
                        claimName: claimName,
                        claimedAt: new Date,
                        claimData: claimData,
                        claimTokenHash: sha256(receipt._id + '/' + claimToken)
                    }
                }
            });

            return {
                statusCode: 200,
                body: {
                    status: 'ok',
                    message: 'ok',
                    result: {
                        'claimToken': claimToken
                    }
                }
            };
        } else {
            return statusCodes.incorrectReceiptKey;
        }
    } catch (e) {
        console.log(e);
        return statusCodes.processingError;
    }
};

export const revokeClaimInvoice = function(checkReceiptKey = true) {
    try {
        let receiptId, receiptKey, params;
        if (this.urlParams && this.urlParams.id) {
            receiptId = this.urlParams.id;
            receiptKey = this.urlParams.key;
            params = _.clone(this.urlParams);
        } else {
            params = _.clone(this.bodyParams);
            if (params && params.id) {
                let id = params.id.split('/');
                receiptId = id[0];
                receiptKey = id[1];
            }
        }

        if (!receiptId || (checkReceiptKey && !receiptKey)) {
            return statusCodes.incompleteRequest;
        }

        let receipt = Receipts.findOne({_id: receiptId});
        if (receipt && (!checkReceiptKey || sha256(receiptKey) === receipt.receiptKeyHash)) {
            const claimToken = params.claimToken;
            const claimType = params.claimType;
            const claimId = params.claimId;

            if (!claimToken || !claimType || !claimId) {
                return statusCodes.incompleteRequest;
            }

            let claim = _.find(receipt.claims, (receiptClaim) => {
                return receiptClaim.claimType === claimType;
            });

            if (!claim) {
                return statusCodes.processingError;
            }

            const claimTokenHash = sha256(receiptId + '/' + claimToken);
            if (claim.claimTokenHash !== claimTokenHash) {
                return statusCodes.dataInvalid;
            }

            // all checks pass, remove claim from claims
            Receipts.update({
                _id: receiptId
            },{
                $pull: {
                    claims: {
                        claimId,
                        claimType,
                        claimTokenHash
                    }
                }
            });

            return {
                statusCode: 200,
                body: {
                    status: 'ok',
                    message: 'claim on invoice has been revoked',
                    result: {
                        'id': receiptId,
                        claimId,
                        claimType
                    }
                }
            };
        } else {
            return statusCodes.incorrectReceiptKey;
        }
    } catch (e) {
        console.log(e);
        return statusCodes.processingError;
    }
};
