import moment from 'moment';
import { getInvoice, getInvoices, addFileToInvoice } from './invoices';
import { claimInvoice, revokeClaimInvoice } from './invoices/claim';
import { searchInvoices } from './invoices/search';

Api.addRoute('admin/invoice/claim', {
    authRequired: true
}, {
    post: {
        authRequired: true,
        action: function() {
            return claimInvoice.call(this, false);
        }
    }
});

Api.addRoute('admin/invoice/claim/revoke', {
    authRequired: true
}, {
    post: {
        authRequired: true,
        action: function() {
            return revokeClaimInvoice.call(this, false);
        }
    }
});

Api.addRoute('admin/invoice/search/:searchTerm', {
    authRequired: true
}, {
    get: {
        authRequired: true,
        action: function() {
            return searchInvoices.call(this, false);
        }
    }
});

Api.addRoute('admin/invoice/search', {
    authRequired: true
}, {
    post: {
        authRequired: true,
        action: function() {
            return searchInvoices.call(this, false);
        }
    }
});

Api.addRoute('admin/invoice/:id', {
    authRequired: true
}, {
    get: {
        authRequired: true,
        action: function() {
            return getInvoice.call(this, false);
        }
    }
});

Api.addRoute('admin/retailer/invoices/:retailerId/:month', {
    authRequired: true
}, {
    get: {
        authRequired: true,
        action: function() {
            // TODO: check whether callee has security access to invoices of retailer
            const retailerId = this.urlParams.retailerId;

            const mMonth = moment(this.urlParams.month, 'YYYYMM');
            const selector = {
                retailerId: retailerId,
                time: {
                    $gte: Number(mMonth.startOf('month').format('X')),
                    $lte: Number(mMonth.endOf('month').format('X'))
                }
            };
            return getInvoices.call(this, selector);
        }
    }
});

Api.addRoute('admin/retailer/invoices/:retailerId/:shopId/:month', {
    authRequired: true
}, {
    get: {
        authRequired: true,
        action: function() {
            // TODO: check whether callee has security access to invoices of retailer
            const retailerId = this.urlParams.retailerId;
            const shopId = this.urlParams.shopId;

            const mMonth = moment(this.urlParams.month, 'YYYYMM');
            const selector = {
                retailerId: retailerId,
                shopId: shopId,
                time: {
                    $gte: Number(mMonth.startOf('month').format('X')),
                    $lte: Number(mMonth.endOf('month').format('X'))
                }
            };
            return getInvoices.call(this, selector);
        }
    }
});

Api.addRoute('admin/invoice/file/add', {
    authRequired: true
}, {
    post: {
        authRequired: true,
        action: function() {
            return addFileToInvoice.call(this, false);
        }
    }
});
