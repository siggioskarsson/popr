import { Random } from 'meteor/random';
import { receiptHash, sha256 } from 'meteor/popr:util/hash';
import { createInvoice, getInvoice, revokeInvoice } from './invoices';
import { claimInvoice, revokeClaimInvoice } from './invoices/claim';
import { getStats, getStatsPerShop, getScanStats } from './stats';
import { getRetailers } from './retailers';

Api.addRoute('invoice/:id/:key', {
    authRequired: false
}, {
    get: {
        authRequired: false,
        action: getInvoice
    }
});

Api.addRoute('invoice/revoke/:id/:key/:hashSignature', {
    authRequired: false
}, {
    get: {
        authRequired: false,
        action: revokeInvoice
    }
});

Api.addRoute('invoice/revoke', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: revokeInvoice
    }
});

Api.addRoute('invoice/claim', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: claimInvoice
    }
});

Api.addRoute('invoice/claim/revoke', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: revokeClaimInvoice
    }
});

Api.addRoute('invoice', {
    authRequired: false
}, {
    put: {
        authRequired: false,
        action: createInvoice
    },
    post: {
        authRequired: false,
        action: createInvoice
    },
    get: {
        authRequired: false,
        action: getInvoice
    }
});

Api.addRoute('stats/:period/:retailerId', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: getStats
    }
});

Api.addRoute('stats/shops/:fromDate/:tillDate/:retailerId', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: getStatsPerShop
    }
});
Api.addRoute('stats/scans/:fromDate/:tillDate/:aggregate', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: getScanStats
    }
});

Api.addRoute('retailers', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: getRetailers
    }
});
