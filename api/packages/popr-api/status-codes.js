/* global statusCodes: true */

statusCodes = {};

statusCodes.ok = {
    statusCode: 200,
    body: {
        status: "ok",
        message: "ok"
    }
};

statusCodes.processingError = {
    statusCode: 500,
    body: {
        status: "error",
        message: "Something went wrong in the processing"
    }
};

statusCodes.incorrectInput = {
    statusCode: 500,
    body: {
        status: "error",
        message: "Incorrect input"
    }
};

statusCodes.noAccess = {
    statusCode: 404,
    body: {
        status: "error",
        message: "No access"
    }
};

statusCodes.retailerNotFound = {
    statusCode: 404,
    body: {
        status: "error",
        message: "Retailer could not be found"
    }
};

statusCodes.noApiAccess = {
    statusCode: 403,
    body: {
        status: "error",
        message: "This nest does not allow api access"
    }
};

statusCodes.incorrectReceiptKey = {
    statusCode: 403,
    body: {
        status: "error",
        message: "Incorrect receipt key given"
    }
};

statusCodes.incompleteRequest = {
    statusCode: 403,
    body: {
        status: "error",
        message: "Not all required data was transmitted with the request"
    }
};

statusCodes.dataInvalid = {
    statusCode: 403,
    body: {
        status: "error",
        message: "Data is invalid"
    }
};

statusCodes.hashSignatureInvalid = {
    statusCode: 403,
    body: {
        status: "error",
        message: "Hash signature is invalid"
    }
};

statusCodes.ncpInvalid = {
    statusCode: 403,
    body: {
        status: "error",
        message: "NCP data is invalid"
    }
};

statusCodes.hashError = {
    statusCode: 403,
    body: {
        status: "error",
        message: "Incorrect hash sent with invoice"
    }
};

statusCodes.receiptRevoked = {
    statusCode: 403,
    body: {
        status: "error",
        message: "Receipt has been revoked and cannot be modified"
    }
};

statusCodes.receiptExists = {
    statusCode: 406,
    body: {
        status: "error",
        message: "Receipt already exists in our system"
    }
};

statusCodes.receiptAlreadyClaimed = {
    statusCode: 407,
    body: {
        status: "error",
        message: "Receipt has already been claimed"
    }
};

statusCodes.claimDataInvalid = {
    statusCode: 407,
    body: {
        status: "error",
        message: "Claim data is invalid. Only keys with string values allowed, with a maximum of 32 key value pairs"
    }
};

statusCodes.notAcceptable = {
    statusCode: 406,
    body: {
        status: "error",
        message: "Not Acceptable"
    }
};

statusCodes.notImplemented = {
    statusCode: 500,
    body: {
        status: "error",
        message: "This function has not yet been implemented"
    }
};
