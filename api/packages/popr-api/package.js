Package.describe({
    name: 'popr:api',
    summary: 'POPr api package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'check',
        'underscore',
        'random',
        'accounts-password',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'meteorhacks:unblock@1.1.0',
        'nimble:restivus@0.8.12',
        'popr:receipts'
    ]);

    // shared files
    api.addFiles([
    ]);

    // server files
    api.addFiles([
        'status-codes.js',
        'server/restivus.js',
        'server/api.js',
        'server/admin-api.js'
    ], 'server');

    // client files
    api.addFiles([
    ], 'client');

    api.export([
    ], 'server'); // temp, only on the server
});

Package.onTest(function(api) {
    api.use([
        'popr:util'
    ]);

    api.use([
        'ecmascript',
        'http',
        'check',
        'underscore',
        'random',
        'accounts-password',
        'meteortesting:mocha',
        'practicalmeteor:sinon',
        'practicalmeteor:chai',
        'hwillson:stub-collections',
        'popr:receipts',
        'popr:api'
    ]);

    api.addAssets([
    ], 'server');

    api.addFiles([
        'tests/data.js',
        'tests/api.js',
        'tests/admin-api.js'
    ], 'server');

    api.export([
    ]);
});
