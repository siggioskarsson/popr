import StubCollections from 'meteor/hwillson:stub-collections';
import { receiptHash, sha256 } from 'meteor/popr:util/hash';
import { Random } from 'meteor/random';
import { claimedInvoice, retailer, unclaimedInvoice } from './data';

const localUrl = "http://localhost:10082/";

const login = function(callback) {
    HTTP.post(localUrl + '/login', {
        data: {
            email: 'support@vatfree.com',
            password: 'test123'
        }
    }, (err, result) => {
        callback(err, result.data.data);
    });
};

describe('admin-api get invalids', function () {
    it('get invoice - not authenticated', function (done) {
        HTTP.get(localUrl + 'admin/invoice/NPM9SABQhG5PQmTEt', (err, res) => {
            expect(err).to.be.a('object');
            expect(res).to.be.a('object');
            expect(res.statusCode).to.be.equal(401);

            done();
        });
    });
});

describe('admin-api get', function () {
    beforeEach(function () {
        StubCollections.add([Meteor.users, Receipts]);
        StubCollections.stub();

        Accounts.createUser({email: 'support@vatfree.com', password: 'test123'});

        Retailers.insert(retailer);

        // already prepared invoice
        Receipts.remove({});
        Receipts.insert(claimedInvoice);
    });

    afterEach(function () {
        StubCollections.restore();
    });

    it('get invoice', function (done) {
        login((err, token) => {
            expect(err).to.be.equal(null);

            HTTP.get(localUrl + 'admin/invoice/' + claimedInvoice._id, {
                headers: {
                    'X-Auth-Token': token.authToken,
                    'X-User-Id': token.userId
                }
            }, (err, res) => {
                expect(err).to.be.equal(null);
                expect(res).to.be.a('object');

                expect(res.statusCode).to.be.equal(200);

                expect(res.data.result.invoice._id).to.be.equal(claimedInvoice._id);
                expect(res.data.result.invoice.claims).to.be.a('array');

                done();
            });
        });
    });

    it('search invoice', function (done) {
        login((err, token) => {
            expect(err).to.be.equal(null);

            HTTP.get(localUrl + 'admin/invoice/search/' + claimedInvoice._id, {
                headers: {
                    'X-Auth-Token': token.authToken,
                    'X-User-Id': token.userId
                }
            }, (err, res) => {
                expect(err).to.be.equal(null);
                expect(res).to.be.a('object');

                expect(res.statusCode).to.be.equal(200);

                expect(res.data.result).to.be.a('array');
                expect(res.data.result[0]._id).to.be.equal(claimedInvoice._id);
                expect(res.data.result[0].claims).to.be.a('array');

                done();
            });
        });
    });

    it('search partial id - GET', function (done) {
        login((err, token) => {
            expect(err).to.be.equal(null);

            HTTP.get(localUrl + 'admin/invoice/search/' + claimedInvoice._id.substr(0, 5).toLowerCase(), {
                headers: {
                    'X-Auth-Token': token.authToken,
                    'X-User-Id': token.userId
                }
            }, (err, res) => {
                expect(err).to.be.equal(null);
                expect(res).to.be.a('object');

                expect(res.statusCode).to.be.equal(200);

                expect(res.data.result).to.be.a('array');
                expect(res.data.result[0]._id).to.be.equal(claimedInvoice._id);
                expect(res.data.result[0].claims).to.be.a('array');

                done();
            });
        });
    });

    it('search partial id - POST', function (done) {
        login((err, token) => {
            expect(err).to.be.equal(null);

            HTTP.post(localUrl + 'admin/invoice/search', {
                headers: {
                    'X-Auth-Token': token.authToken,
                    'X-User-Id': token.userId
                },
                data: {
                    searchTerm: claimedInvoice._id.substr(0, 5).toLowerCase()
                }
            }, (err, res) => {
                expect(err).to.be.equal(null);
                expect(res).to.be.a('object');

                expect(res.statusCode).to.be.equal(200);

                expect(res.data.result).to.be.a('array');
                expect(res.data.result[0]._id).to.be.equal(claimedInvoice._id);
                expect(res.data.result[0].claims).to.be.a('array');

                done();
            });
        });
    });
});

describe('admin-api claim', function () {
    beforeEach(function () {
        StubCollections.add([Meteor.users, Receipts]);
        StubCollections.stub();

        Accounts.createUser({email: 'support@vatfree.com', password: 'test123'});

        Retailers.insert(retailer);

        // already prepared invoice
        Receipts.remove({});
        const claimedInvoiceInsert = JSON.parse(JSON.stringify(claimedInvoice));
        delete claimedInvoiceInsert.claims;
        Receipts.insert(claimedInvoiceInsert);
    });

    afterEach(function () {
        StubCollections.restore();
    });

    it('get invoice', function (done) {
        login((err, token) => {
            expect(err).to.be.equal(null);

            HTTP.get(localUrl + 'admin/invoice/' + claimedInvoice._id, {
                headers: {
                    'X-Auth-Token': token.authToken,
                    'X-User-Id': token.userId
                }
            }, (err, res) => {
                expect(err).to.be.equal(null);
                expect(res).to.be.a('object');

                expect(res.statusCode).to.be.equal(200);

                expect(res.data.result.invoice._id).to.be.equal(claimedInvoice._id);
                expect(res.data.result.invoice.claims).to.be.equal(undefined);

                done();
            });
        });
    });

    it('admin claim invoice', function (done) {
        login((err, token) => {
            expect(err).to.be.equal(null);

            HTTP.post(localUrl + 'admin/invoice/claim', {
                headers: {
                    'X-Auth-Token': token.authToken,
                    'X-User-Id': token.userId
                },
                data: {
                    id: claimedInvoice._id,
                    claim: 'vat-refund',
                    claimId: 'popr_test',
                    claimName: "POPR test"
                }
            }, (err, res) => {
                expect(err).to.be.equal(null);

                const claimToken = res.data.result.claimToken;
                expect(claimToken).to.be.a('string');

                HTTP.get(localUrl + 'admin/invoice/' + claimedInvoice._id, {
                    headers: {
                        'X-Auth-Token': token.authToken,
                        'X-User-Id': token.userId
                    }
                }, (err, res) => {
                    const invoice = res.data.result.invoice;

                    expect(invoice._id).to.be.a('string');
                    expect(invoice._id).to.not.be.equal("setIdToSomething");
                    expect(invoice.hashSignature).to.be.equal(undefined);

                    expect(invoice.retailerId).to.be.equal("tkmWjDednjSfcWSgC");
                    expect(invoice.ncp).to.be.a('string');
                    // since we did not send the receipt key, we cannot decrypt the ncp data
                    //expect(invoice.ncp.name).to.be.equal("Doe, John Wilkens");

                    expect(invoice.claims).to.be.a('array');
                    expect(invoice.claims[0]).to.be.a('object');
                    expect(invoice.claims[0].claimType).to.be.equal('vat-refund');
                    expect(invoice.claims[0].claimId).to.be.equal('popr_test');
                    expect(invoice.claims[0].claimName).to.be.equal('POPR test');
                    expect(invoice.claims[0].claimedAt).to.be.a('string');
                    expect(invoice.claims[0].claimData).to.be.equal(null);
                    expect(invoice.claims[0].claimTokenHash).to.be.a('string');

                    done();
                });
            });
        });
    });

    it('admin revoke claim on existing invoice', function (done) {
        login((err, token) => {
            expect(err).to.be.equal(null);

            const id = claimedInvoice._id;
            HTTP.post(localUrl + 'admin/invoice/claim', {
                headers: {
                    'X-Auth-Token': token.authToken,
                    'X-User-Id': token.userId
                },
                data: {
                    id: id,
                    claim: 'vat-refund',
                    claimId: 'popr_test',
                    claimName: "POPR test"
                }
            }, (err, res) => {
                expect(err).to.be.equal(null);

                const claimToken = res.data.result.claimToken;
                expect(claimToken).to.be.a('string');

                HTTP.post(localUrl + 'admin/invoice/claim/revoke', {
                    headers: {
                        'X-Auth-Token': token.authToken,
                        'X-User-Id': token.userId
                    },
                    data: {
                        id,
                        claimType: 'vat-refund',
                        claimId: 'popr_test',
                        claimToken
                    }
                }, (err, res) => {
                    expect(err).to.be.equal(null);

                    const receiptId = id.split('/')[0];

                    expect(res.data).to.be.a('object');
                    expect(res.data.result).to.be.a('object');
                    expect(res.data.result.id).to.be.equal(receiptId);
                    expect(res.data.result.claimId).to.be.equal('popr_test');
                    expect(res.data.result.claimType).to.be.equal('vat-refund');

                    HTTP.get(localUrl + 'admin/invoice/' + id, {
                        headers: {
                            'X-Auth-Token': token.authToken,
                            'X-User-Id': token.userId
                        }
                    }, (err, res) => {
                        expect(err).to.be.equal(null);

                        const invoice = res.data.result.invoice;

                        expect(invoice.claims).to.be.a('array');
                        expect(invoice.claims.length).to.be.equal(0);

                        done();
                    });
                });
            });
        });
    });
});
