export const claimedInvoice = {
    "_id": "NPM9SABQhG5PQmTEt",
    "time": 1520856658,
    "retailerId": "tkmWjDednjSfcWSgC",
    "shopId": "1",
    "terminalId": "ams2345-33",
    "invoiceId": "WadkoR2pbwP4qe34F",
    "totalValue": 21995,
    "totalVAT": 3818,
    "vatLines": [
        {
            "vat": 2100,
            "vatAmount": 3818
        },
        {
            "vat": 600,
            "vatAmount": 0
        }
    ],
    "ncp": "859b868bce0ae74c590d96b04da4c221ce26065352ed1b0ebaa1c1e96280a32b3f09b6a46eabdb4bc648e57d5252f758a199770d3b43823202070e1e247f18995b420302f3a4c1153b8e239eb4e80570c7ad82b0975e3cb5c2a9f95f5afbe4927d06a7bcca9f172dd4f63cd7369654f6",
    "hash": "232f33559c5dd09020ac30c69d7a66bc49cac3a18d3db7aa4bb5207e93ae4a38",
    "receiptKeyHash": "7550f9ca0472d100a1d9fba5ead23540cbe7ba4f8ce04f1f02950e14e85738fd",
    "createdAt": new Date(1520856968629),
    "claims": [
        {
            "claimType": "vat-refund",
            "claimId": "vatfree:LhFAgACJsH3CjRPrn",
            "claimedAt": new Date(1520866986743),
            "claimData": {
                "receiptId": "LhFAgACJsH3CjRPrn",
                "shopId": "ao3vH7XmHxRvbnHxx"
            },
            "claimTokenHash": "15c9a87a198376df060f45cb52d4ad1f0ec5aa8ec38c22175e37962c547a8c8a"
        }
    ]
};

export const unclaimedInvoice = {
    "_id": "setIdToSomething",
    "time": 1520856659,
    "retailerId": "tkmWjDednjSfcWSgC",
    "shopId": "2",
    "terminalId": "ams2345-34",
    "invoiceId": "WadkoR2pbwP4qe34G",
    "totalValue": 21995,
    "totalVAT": 3818,
    "vatLines": [
        {
            "vat": 2100,
            "vatAmount": 3818
        },
        {
            "vat": 600,
            "vatAmount": 0
        }
    ],
    "ncp": {
        "name": "Doe, John Wilkens",
        "country": "US",
        "passportNr": "AV2342535",
        "passportExpiration": "2018-01-01"
    }
};

export const retailer = {
    "_id": "tkmWjDednjSfcWSgC",
    "name": "Heineken Experience Store",
    "logoImg": "/images/heineken@2x.png",
    "publicOptions": {
        "showEstimatedRefund": true
    },
    "hashSecret": "8d1439e789f46f3493a23be59701ed0c81ae4fee6c6b71ad15cc5630dec3cf4f"
};
