import StubCollections from 'meteor/hwillson:stub-collections';
import { receiptHash, sha256 } from 'meteor/popr:util/hash';
import { Random } from 'meteor/random';
import { claimedInvoice, retailer, unclaimedInvoice } from './data';

const localUrl = "http://localhost:10082/";

describe('api get invalids', function () {
    it('get invalid invoice', function (done) {
        HTTP.get(localUrl + 'invoice/test/invoice', (err, res) => {
            expect(err).to.be.a('object');
            expect(res).to.be.a('object');
            expect(res.statusCode).to.be.equal(403);

            done();
        });
    });

    it('get non existing invoice', function (done) {
        HTTP.get(localUrl + 'invoice/NPM9SABQhG5PQmTEt/sZS7TMauw4Zqn_H1AoSMtmg3O1A_XyYz1JKpZHbkSdkfNJEfR0tq1Lw3bE_l2Zae', (err, res) => {
            expect(err).to.be.a('object');
            expect(res).to.be.a('object');
            expect(res.statusCode).to.be.equal(403);

            done();
        });
    });
});

describe('api get valids', function () {
    beforeEach(function() {
        StubCollections.add([Receipts]);
        StubCollections.stub();

        // already prepared invoice
        Receipts.remove({});
        Receipts.insert(claimedInvoice);
    });
    afterEach(function() {
        StubCollections.restore();
    });

    it('get existing invoice', function (done) {
        HTTP.get(localUrl + 'invoice/NPM9SABQhG5PQmTEt/sZS7TMauw4Zqn_H1AoSMtmg3O1A_XyYz1JKpZHbkSdkfNJEfR0tq1Lw3bE_l2Zae', (err, res) => {
            expect(err).to.be.null;
            expect(res).to.be.a('object');
            expect(res.statusCode).to.be.equal(200);

            let content = JSON.parse(res.content);
            expect(content).to.be.a('object');
            expect(content.status).to.be.equal('ok');
            expect(content.message).to.be.equal('ok');

            expect(content.result).to.be.a('object');
            expect(content.result.invoice).to.be.a('object');

            let invoice = content.result.invoice;
            expect(invoice.retailerId).to.be.equal("tkmWjDednjSfcWSgC");

            // test decrypt of ncp data
            expect(invoice.ncp).to.not.be.a('string');
            expect(invoice.ncp.name).to.be.equal("Doe, John Wilkens");
            expect(invoice.ncp.country).to.be.equal("US");
            expect(invoice.ncp.passportNr).to.be.equal("AV2342535");
            expect(invoice.ncp.passportExpiration).to.be.equal("2018-01-01");

            // test whether claims were included
            expect(invoice.claims).to.be.a('array');
            expect(invoice.claims[0].claimType).to.be.equal("vat-refund");

            done();
        });
    });

    it('get invoice without secret', function (done) {
        HTTP.get(localUrl + 'invoice/NPM9SABQhG5PQmTEt/erfgsergwersgaergaergaerg', (err, res) => {
            expect(err).to.be.a('object');
            expect(err.response.data.message).to.be.equal('Incorrect receipt key given');

            done();
        });
    });

    it('get invoice without secret', function (done) {
        HTTP.get(localUrl + 'invoice/NPM9SABQhG5PQmTEt', (err, res) => {
            expect(res.content).to.be.a('string');
            expect(res.data).to.be.equal(null);

            done();
        });
    });
});

describe('create and check', function () {
    let insertedReceipt = {};
    let insertedReceiptResult = {};
    beforeEach(function() {
        StubCollections.add([Receipts, Retailers]);
        StubCollections.stub();

        Retailers.insert(retailer);

        const receiptData = JSON.parse(JSON.stringify(unclaimedInvoice));
        receiptData.hash = receiptHash(receiptData);
        receiptData.hashSignature = sha256(receiptData.hash + retailer.hashSecret);

        // already prepared invoice
        insertedReceiptResult = HTTP.post(localUrl + 'invoice', {
            data: receiptData
        });

        insertedReceipt = receiptData;
    });

    afterEach(function() {
        StubCollections.restore();
    });

    it('get existing invoice', function (done) {
        HTTP.get(localUrl + 'invoice/' + insertedReceiptResult.data.result.id, (err, res) => {
            const invoice = res.data.result.invoice;
            expect(invoice._id).to.be.a('string');
            expect(invoice._id).to.not.be.equal("setIdToSomething");
            expect(invoice.hashSignature).to.be.equal(undefined);

            expect(invoice.retailerId).to.be.equal("tkmWjDednjSfcWSgC");
            expect(invoice.ncp).to.be.a('object');
            expect(invoice.ncp.name).to.be.equal("Doe, John Wilkens");
            done();
        });
    });

    it('claim existing invoice', function (done) {
        HTTP.post(localUrl + 'invoice/claim', {
            data: {
                id: insertedReceiptResult.data.result.id,
                claim: 'vat-refund',
                claimId: 'popr_test',
                claimName: "POPR test"
            }
        }, (err, res) => {
            expect(err).to.be.equal(null);

            const claimToken = res.data.result.claimToken;
            expect(claimToken).to.be.a('string');

            HTTP.get(localUrl + 'invoice/' + insertedReceiptResult.data.result.id, (err, res) => {
                const invoice = res.data.result.invoice;

                expect(invoice._id).to.be.a('string');
                expect(invoice._id).to.not.be.equal("setIdToSomething");
                expect(invoice.hashSignature).to.be.equal(undefined);

                expect(invoice.retailerId).to.be.equal("tkmWjDednjSfcWSgC");
                expect(invoice.ncp).to.be.a('object');
                expect(invoice.ncp.name).to.be.equal("Doe, John Wilkens");

                expect(invoice.claims).to.be.a('array');
                expect(invoice.claims[0]).to.be.a('object');
                expect(invoice.claims[0].claimType).to.be.equal('vat-refund');
                expect(invoice.claims[0].claimId).to.be.equal('popr_test');
                expect(invoice.claims[0].claimName).to.be.equal('POPR test');
                expect(invoice.claims[0].claimedAt).to.be.a('string');
                expect(invoice.claims[0].claimData).to.be.equal(null);
                expect(invoice.claims[0].claimTokenHash).to.be.a('string');

                done();
            });
        });
    });

    it('revoke existing invoice', function (done) {
        HTTP.post(localUrl + 'invoice/revoke', {
            data: {
                id: insertedReceiptResult.data.result.id,
                hashSignature: sha256(insertedReceipt.hash + 'revoke' + retailer.hashSecret)
            }
        }, (err, res) => {
            expect(err).to.be.equal(null);

            HTTP.get(localUrl + 'invoice/' + insertedReceiptResult.data.result.id, (err, res) => {
                const invoice = res.data.result.invoice;

                expect(invoice.revokedAt).to.be.a('string');
                expect(invoice.revokedBy).to.be.a('string');

                // try to claim the invoice, this should fail
                HTTP.post(localUrl + 'invoice/claim', {
                    data: {
                        id: insertedReceiptResult.data.result.id,
                        claim: 'vat-refund',
                        claimId: 'popr_test',
                        claimName: "POPR test"
                    }
                }, (err, res) => {
                    expect(err).to.be.a('object');
                    expect(err.response.data.message).to.be.equal('Receipt has been revoked and cannot be modified');

                    done();
                });
            });
        });
    });

    it('revoke claim on existing invoice', function (done) {
        HTTP.post(localUrl + 'invoice/claim', {
            data: {
                id: insertedReceiptResult.data.result.id,
                claim: 'vat-refund',
                claimId: 'popr_test',
                claimName: "POPR test"
            }
        }, (err, res) => {
            expect(err).to.be.equal(null);

            const claimToken = res.data.result.claimToken;
            expect(claimToken).to.be.a('string');

            const id = insertedReceiptResult.data.result.id;

            HTTP.post(localUrl + 'invoice/claim/revoke', {
                data: {
                    id,
                    claimType: 'vat-refund',
                    claimId: 'popr_test',
                    claimToken
                }
            }, (err, res) => {
                expect(err).to.be.equal(null);

                const receiptId = id.split('/')[0];

                expect(res.data).to.be.a('object');
                expect(res.data.result).to.be.a('object');
                expect(res.data.result.id).to.be.equal(receiptId);
                expect(res.data.result.claimId).to.be.equal('popr_test');
                expect(res.data.result.claimType).to.be.equal('vat-refund');

                HTTP.get(localUrl + 'invoice/' + id, (err, res) => {
                    const invoice = res.data.result.invoice;

                    expect(invoice.claims).to.be.a('array');
                    expect(invoice.claims.length).to.be.equal(0);

                    done();
                });
            });
        });
    });
});
